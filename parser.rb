require 'faraday'
require 'nokogiri'
require 'pry'

def cls
  if Gem.win_platform?
    system('cls') # windows
  else
    system('reset') # linux
  end
end

b = Faraday.get('https://twitter.com/elonmusk')

doc = Nokogiri::HTML(b.body)
posts = {}

#search tweets and fill in the hash
doc.xpath("*//li[@data-item-type='tweet']").each do |div|

  post_id = div.children[1].attributes["data-tweet-id"].value
  post_link = div.children[1].attributes["data-permalink-path"].value
  post_text = div.xpath("*//div[@class='js-tweet-text-container']").children.children.first.text
  post_ltr = div.xpath("*//div[@dir='ltr']").text
  post_date = div.xpath("*//a[@href='#{post_link}']").first.attributes['title'].value
  post_likes = div.xpath("*//span[@id='profile-tweet-action-favorite-count-aria-#{post_id}']").first.children.text

  posts[post_id] = {}
  posts[post_id][:link] = post_link
  posts[post_id][:text] = post_text
  posts[post_id][:date] = post_date.split(' - ').last
  posts[post_id][:likes] = post_likes
  posts[post_id][:ltr] = post_ltr

  break if posts.keys.size >= 3

end

cls #clear screen
posts.each_key do |item|
  puts posts[item][:date]
  puts
  puts posts[item][:text]
  unless posts[item][:ltr] == ''
    puts
    puts posts[item][:ltr]
  end
  puts
  puts posts[item][:likes]
  puts '==========================================================='
end