Доброго времени. 

Не смог получить API-token для twitter. На запрос токенамне пришёл отказ. По другому не смог авторизоваться в twitter.
Пробовал basic_auth и post-запрос на https://twitter.com/sessions с параметрами:
session[username_or_email]: app.tvitter@bk.ru
session[password]: <passwors>
authenticity_token: 3d2243c0622111eaa1b69fe55478a6dd (каждый раз разное значение, это для примера)

versions:
ruby 2.4.9
gem faraday (1.0.0)
gem nokogiri (1.10.9)